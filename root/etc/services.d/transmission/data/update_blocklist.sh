#!/bin/sh
# Switch to execution dir
cd "$(dirname "$0")" || exit 1
# Initial post
cat <post.req - | nc localhost "${TRANSMISSION_RPC_PORT}" > post.res
# Check if HTTP error 409 was returned
if head -1 post.res | grep -q "HTTP/1.1 409 Conflict"; then
    # Session ID is invalid so grab from response
    SESSION_ID=$(grep -m 1 X-Transmission-Session-Id post.res)
    # Replace in post
    sed -i -r "s#X-Transmission-Session-Id: .*#${SESSION_ID}#" post.req
    # Follow up post
    cat <post.req - | nc localhost "${TRANSMISSION_RPC_PORT}" > post.res
fi
# Switch back
cd - >/dev/null || exit
