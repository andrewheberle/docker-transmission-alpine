REGISTRY = registry.gitlab.com/andrewheberle
IMAGE_NAME = docker-transmission-alpine

BUILD_TAG := $(shell git describe --long --tags)
BUILD_IMAGE_NAME = $(REGISTRY)/$(IMAGE_NAME):$(BUILD_TAG)

# Image tags
BUILD_TAG_BARE := $(shell echo $(BUILD_TAG) | cut -d- -f1)
MAJOR_VERSION := $(shell echo $(BUILD_TAG_BARE) | cut -d. -f1)
MINOR_VERSION := $(shell echo $(BUILD_TAG_BARE) | cut -d. -f2)
PATCH_VERSION := $(shell echo $(BUILD_TAG_BARE) | cut -d. -f3)
FULL_VERSION := $(shell git describe --tags)
MAJOR_TAG = $(REGISTRY)/$(IMAGE_NAME):$(MAJOR_VERSION)
MINOR_TAG = $(REGISTRY)/$(IMAGE_NAME):$(MAJOR_VERSION).$(MINOR_VERSION)
FULL_TAG = $(REGISTRY)/$(IMAGE_NAME):$(MAJOR_VERSION).$(MINOR_VERSION).$(PATCH_VERSION)
FULL_VERSION_TAG = $(REGISTRY)/$(IMAGE_NAME):$(FULL_VERSION)
LATEST = $(REGISTRY)/$(IMAGE_NAME):latest


.PHONY: build tag-version tag-latest tag publish-version publish-latest publish release help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build: Dockerfile ## Build and tag image.
	docker build $(DOCKER_BUILD_OPTS) \
		-t $(BUILD_IMAGE_NAME) .
	$(MAKE) tag

tag-version: ## Tag image as '$(MAJOR_TAG)', '$(MINOR_TAG)' and '$(FULL_TAG)'.
	docker tag $(BUILD_IMAGE_NAME) $(MAJOR_TAG)
	docker tag $(BUILD_IMAGE_NAME) $(MINOR_TAG)
	docker tag $(BUILD_IMAGE_NAME) $(FULL_TAG)
	docker tag $(BUILD_IMAGE_NAME) $(FULL_VERSION_TAG)

tag-latest: ## Tag image as 'latest'.
	docker tag $(BUILD_IMAGE_NAME) $(LATEST)

tag: tag-version tag-latest ## Tag image.

publish-version: tag-version ## Push images tagged as '$(MAJOR_TAG)', '$(MINOR_TAG)' and '$(FULL_TAG)'.
	docker push $(MAJOR_TAG)
	docker push $(MINOR_TAG)
	docker push $(FULL_TAG)
	docker push $(FULL_VERSION_TAG)

publish-latest: tag-latest ## Push images tagged as 'latest'.
	docker push $(LATEST)

publish: publish-version publish-latest ## Push images.

release: check-release-env build publish ## Build and push images.

check-release-env:
ifndef REGISTRY
	$(error REGISTRY is not defined)
endif
