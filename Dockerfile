FROM registry.gitlab.com/andrewheberle/docker-base:alpine3.14

ENV TRANSMISSION_CONF_DIR="/var/lib/transmission/config" \
    TRANSMISSION_DOWNLOAD_DIR="/var/lib/transmission/downloads" \
    TRANSMISSION_ADMIN_USER="admin" \
    TRANSMISSION_ADMIN_PASSWORD="admin" \
    TRANSMISSION_ALLOWED="127.0.0.1" \
    TRANSMISSION_PEER_PORT="51413" \
    TRANSMISSION_RPC_PORT="9091" \
    TRANSMISSION_RPC_HOST_WHITELIST="" \
    TRANSMISSION_OPTS="--encryption-required" \
    TRANSMISSION_BLOCKLIST_URL="" \
    SERVICE_NAME="transmission" \
    SERVICE_USER="transmission" \
    SERVICE_GROUP="transmission" \
    SERVICE_HOME="/var/lib/transmission"

# Install packages
RUN apk --no-cache add ca-certificates transmission-daemon

# Copy root fs skeleton
COPY root /

EXPOSE $TRANSMISSION_PEER_PORT/tcp $TRANSMISSION_PEER_PORT/udp $TRANSMISSION_RPC_PORT/tcp

VOLUME ["$TRANSMISSION_CONF_DIR", "$TRANSMISSION_DOWNLOAD_DIR"]

HEALTHCHECK CMD /etc/services.d/$SERVICE_NAME/data/check || exit 1
